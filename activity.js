// Use the count operator to count the total number of fruits on sale.
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$count: "Total Fruits On Sale"}
]);

// Use the count operator to count the total number of fruits with stock more than 20.
db.fruits.aggregate([
	{$match: {stock: {$gte: 20}}},
	{$count: "Total stocks more than 20"}
]);

// Use the average operator to get the average price of fruits onSale per supplier.
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplierId", averagePrice: {$avg:"$price"}}}
]);


//Use the max operator to get the highest price of a fruit per supplier
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplierId", maxPrice: {$max:"$price"}}}
]);

// Use the min operator to get the lowest price of a fruit per supplier
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplierId", lowestPrice: {$min:"$price"}}}
]);
